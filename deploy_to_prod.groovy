job('DeployToProd') {
  parameters {
    stringParam('TARGET_SERVER', 'undefined', 'IP address of the target server to deploy')
  }
  logRotator {
    numToKeep(20)
  }
  multiscm {
    git {
      remote {
        name('origin')
        url('https://gitlab.com/jobvacancy-java/jobvacancy-scripts.git')
      }
      branch('*/master')
      extensions {
        cleanBeforeCheckout()
      }
    }
    git {
      remote {
        name('origin')
        url('https://gitlab.com/jobvacancy-java/jobvacancy-config.git')
      }
      branch('prod')
      extensions {
        relativeTargetDirectory("""config""")
      }
    }
  }
  steps {
    shell('./deploy_remote.sh')
  }
  publishers {
    downstream('CheckProd')
  }
}