buildPipelineView('Pipeline') {
    filterBuildQueue()
    filterExecutors()
    title('JobVacancyPipeline')
    displayedBuilds(10)
    selectedJob('Build')
    alwaysAllowManualTrigger()
    showPipelineParameters()
    refreshFrequency(60)
}