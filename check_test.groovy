job('CheckTest') {
  parameters {
    stringParam('TARGET_SERVER', 'undefined', 'IP address of the target server to deploy')
  }
  logRotator {
    numToKeep(20)
  }
  multiscm {
    git{
      remote {
        name('origin')
        url('https://gitlab.com/jobvacancy-java/jobvacancy-scripts.git')
      }
      branch('*/master')
      extensions {
        cleanBeforeCheckout()
      }
    }
  }
  steps {
    shell('./check_url.sh $TARGET_SERVER')
  }
  publishers {
    buildPipelineTrigger('DeployToProd')
  }
}