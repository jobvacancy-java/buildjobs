job('Release') {
  logRotator {
    numToKeep(20)
  }
  multiscm {
    git {
      remote {
        name('origin')
        url('https://gitlab.com/jobvacancy-java/jobvacancy-source.git')
      }
      branch('*/beginsolution')
    }
  }
  steps {
    shell('mvn clean package -DskipTests -Pprod')
  }
  publishers {
    downstream('DeployToTest')
  }
}